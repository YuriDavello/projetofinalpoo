create database if not exists Loja_X;
use Loja_X;
drop database Loja_X;

create table if not exists Fornecedor(
	IdFornecedor int not null primary key auto_increment,
    NomeForn varchar(50) not null,
    CNPJ varchar(14) not null,
    EmailForn varchar(25),
    TelefoneForn1 varchar(50) not null,
    TelefoneForn2 varchar(50),
    EnderecoForn varchar(100) not null
);

insert into Fornecedor (NomeForn, CNPJ, EmailForn, TelefoneForn1, TelefoneForn2, EnderecoForn)
values ('Grupo TXT', '41547545441012', 'grupotxt@gr.com', '14781111', '80004874', 'Avenida Margarida, 80, Jardim Lisboa, Guarulhos, 5105-120');
insert into Fornecedor (NomeForn, CNPJ, EmailForn, TelefoneForn1, EnderecoForn)
values ('Marcenaria João', '41547545441012', 'marcjoao@marcjoao.com', '14781111', 'Rua Manoel, 100, Vila Mariana, São Paulo, 1145-110');

create table if not exists Estoque(
	IdProduto int not null primary key auto_increment,
    NomePro varchar(50) not null,
    Valor float not null,
    Quantidade int not null
);

insert into Estoque (NomePro, Valor,Quantidade) 
values ('Notebook Intel 1TB', 2400.55,10);
insert into Estoque (NomePro, Valor,Quantidade) 
values ('Guarda-Roupa 4 Portas',450.99,12);
	
create table if not exists Cliente(
	IdCliente int not null primary key auto_increment,
    NomeCli varchar(50) not null,
    CpfCli varchar(11) not null,
    EmailCli varchar(25),
    EnderecoCli varchar(100) not null,
    TelefoneCli varchar(50) not null
);

insert into Cliente (NomeCli, CpfCli, EmailCli, EnderecoCli, TelefoneCli)
values ('Ana Maria', 45474412377, 'anamaria@gmail.com', 'Rua Lima Barreto, 145, Vila Azul, São Paulo', '1195487541');
insert into Cliente (NomeCli, CpfCli, EmailCli, EnderecoCli, TelefoneCli)
values ('Kuan Marques', '40398221880','kauanmarques@gmail.com', 'Rua Lima Barreto, 145, Vila Azul, São Paulo', '11996054556');

create table if not exists Funcionario(
	IdFuncionario int not null primary key auto_increment,
    NomeFunc varchar(50) not null,
    CpfFunc varchar(11) not null,
    Usuario varchar(25) not null,
    EmailFunc varchar(25),
    TelefoneFunc varchar(50) not null
);

insert into Funcionario (NomeFunc, CpfFunc, Usuario, EmailFunc, TelefoneFunc) 
values ('José Silva', '87421535477', 'josesilva@lojax', 'josesilva@gmail.com', '1191547777');
insert into Funcionario (NomeFunc, CpfFunc, Usuario, EmailFunc, TelefoneFunc) 
values ('Maria Souza', '97845621511', 'mariasouza@lojax', 'mariasouza@gmail.com', '1198744744');

select * from funcionario;
select * from cliente;
select * from estoque;
select * from fornecedor;