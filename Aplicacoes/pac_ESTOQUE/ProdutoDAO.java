package pac_ESTOQUE;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class ProdutoDAO {
	
	private Connection getConnection() throws SQLException, ClassNotFoundException {
		String driver = "com.mysql.cj.jdbc.Driver";
		String usuario = "root";
		String senha = "yuri2011";
		String url = "jdbc:mysql://localhost:3306/Loja_X?useTimezone=true&serverTimezone=UTC";
		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, usuario, senha);
		return conn;
	}
	
	public void Incluir(Produto p) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "insert into "
				+ "Estoque (NomePro, Valor, Quantidade)"
				+ "values(?, ?, ?)";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, p.getNomePro());
		comando.setFloat(2, p.getValorPro());
		comando.setInt(3, p.getQuantPro());
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Produto inclu�do");
	}
	
	public void Alterar(Produto p) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "update Estoque set valor = ?, quantidade = ?"
				+ " where nomepro = ?;";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setFloat(1, p.getValorPro());
		comando.setInt(2, p.getQuantPro());
		comando.setString(3, p.getNomePro());
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Produto alterado");
	}
	
	public void Excluir(String nome) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "delete from Estoque where nomepro = ?;";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, nome);
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Produto deletado");
	}
	
	public Produto Consultar(String nome) throws SQLException, ClassNotFoundException {
		Produto data = new Produto();
		Connection conn = getConnection();
		String Sql = "select * from Estoque where nomepro = ?";
		PreparedStatement comando = conn.prepareStatement(Sql);
		comando.setString(1, nome);
		ResultSet result = comando.executeQuery();
		while(result.next()) {
			data.setNomePro(result.getString("NomePro"));
			data.setValorPro(result.getFloat("Valor"));
			data.setQuantPro(result.getInt("Quantidade"));
		}
		comando.close();
		conn.close();
		return data;
	}

}
