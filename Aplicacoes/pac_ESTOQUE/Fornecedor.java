package pac_ESTOQUE;

public class Fornecedor {
	private String NomeForn;
	private String CNPJ;
	private String EmailForn;
	private String TelefoneForn1;
	private String TelefoneForn2;
	private String EnderecoForn;

	public String getNomeForn() {
		return NomeForn;
	}

	public void setNomeForn(String nomeForn) {
		NomeForn = nomeForn;
	}

	public String getCNPJ() {
		return CNPJ;
	}

	public void setCNPJ(String cNPJ) {
		CNPJ = cNPJ;
	}

	public String getEmailForn() {
		return EmailForn;
	}

	public void setEmailForn(String emailForn) {
		EmailForn = emailForn;
	}

	public String getTelefoneForn1() {
		return TelefoneForn1;
	}

	public void setTelefoneForn1(String telefoneForn1) {
		TelefoneForn1 = telefoneForn1;
	}

	public String getTelefoneForn2() {
		return TelefoneForn2;
	}

	public void setTelefoneForn2(String telefoneForn2) {
		TelefoneForn2 = telefoneForn2;
	}

	public String getEnderecoForn() {
		return EnderecoForn;
	}

	public void setEnderecoForn(String enderecoForn) {
		EnderecoForn = enderecoForn;
	}
}
