package pac_ESTOQUE;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class frmProduto extends JFrame {

	protected JPanel contentPane;
	protected JTextField txtNomePro;
	protected JTextField txtValorPro;
	protected JTextField txtNomeForn;
	protected JTextField txtQuantPro;
	
	private final ProdutoDAO produtoDao = new ProdutoDAO();

	JButton btnIncluir = new JButton("Incluir");
	JButton btnExcluir = new JButton("Excluir");
	JButton btnAlterar = new JButton("Alterar");
	JButton btnConsultar = new JButton("Consultar");
	JButton btnVoltar = new JButton("Voltar");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmProduto frame = new frmProduto();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmProduto() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 613, 384);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblProduto = new JLabel("Produto");
		lblProduto.setHorizontalAlignment(SwingConstants.CENTER);
		lblProduto.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblProduto.setBounds(238, -6, 97, 71);
		contentPane.add(lblProduto);

		JLabel lblNewLabel_1 = new JLabel("Nome:");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(20, 71, 46, 14);
		contentPane.add(lblNewLabel_1);

		txtNomePro = new JTextField();
		txtNomePro.setColumns(10);
		txtNomePro.setBounds(124, 71, 317, 19);
		contentPane.add(txtNomePro);

		JLabel lblNewLabel_2 = new JLabel("Valor:");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(20, 112, 46, 14);
		contentPane.add(lblNewLabel_2);

		txtValorPro = new JTextField();
		txtValorPro.setColumns(10);
		txtValorPro.setBounds(124, 110, 124, 20);
		contentPane.add(txtValorPro);

		JLabel lblNewLabel_4 = new JLabel("Quantidade:");
		lblNewLabel_4.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_4.setBounds(20, 150, 94, 14);
		contentPane.add(lblNewLabel_4);

		txtQuantPro = new JTextField();
		txtQuantPro.setColumns(10);
		txtQuantPro.setBounds(124, 150, 124, 20);
		contentPane.add(txtQuantPro);

		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnVoltar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnVoltar.setBounds(238, 283, 105, 32);
		contentPane.add(btnVoltar);

		btnIncluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Produto p = new Produto();
				p.setNomePro(txtNomePro.getText());
				p.setValorPro(Float.parseFloat(txtValorPro.getText()));
				p.setQuantPro(Integer.parseInt(txtQuantPro.getText()));

				try {
					produtoDao.Incluir(p);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomePro.setText("");
				txtValorPro.setText("");
				txtQuantPro.setText("");
			}
		});
		
		btnIncluir.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnIncluir.setBounds(489, 75, 89, 23);
		contentPane.add(btnIncluir);

		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					produtoDao.Excluir(txtNomePro.getText());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomePro.setText("");
				txtValorPro.setText("");
				txtQuantPro.setText("");
			}
		});
		
		btnExcluir.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnExcluir.setBounds(489, 115, 89, 23);
		contentPane.add(btnExcluir);

		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Produto p = new Produto();
				p.setNomePro(txtNomePro.getText());
				p.setValorPro(Float.parseFloat(txtValorPro.getText()));
				p.setQuantPro(Integer.parseInt(txtQuantPro.getText()));
				try {
					produtoDao.Alterar(p);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomePro.setText("");
				txtValorPro.setText("");
				txtQuantPro.setText("");
			}
		});
		
		btnAlterar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnAlterar.setBounds(489, 155, 89, 23);
		contentPane.add(btnAlterar);

		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Produto produto = produtoDao.Consultar(txtNomePro.getText());
					txtNomePro.setText(produto.getNomePro());
					txtValorPro.setText(String.valueOf(produto.getValorPro()));
					txtQuantPro.setText(String.valueOf(produto.getQuantPro()));
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
			}
		});
		
		btnConsultar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnConsultar.setBounds(489, 192, 89, 23);
		contentPane.add(btnConsultar);
	}

}
