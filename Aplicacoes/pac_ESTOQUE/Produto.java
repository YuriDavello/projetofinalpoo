package pac_ESTOQUE;

public class Produto {
	private String nomePro;
	private float ValorPro;
	private int QuantPro;
	
	
	public String getNomePro() {
		return nomePro;
	}

	public void setNomePro(String nomePro) {
		this.nomePro = nomePro;
	}

	public float getValorPro() {
		return ValorPro;
	}

	public void setValorPro(float valorPro) {
		ValorPro = valorPro;
	}

	public int getQuantPro() {
		return QuantPro;
	}

	public void setQuantPro(int quantPro) {
		QuantPro = quantPro;
	}
}
