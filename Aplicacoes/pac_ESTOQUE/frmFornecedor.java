package pac_ESTOQUE;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

@SuppressWarnings({ "serial", "unused" })

public class frmFornecedor extends JFrame {

	protected JPanel contentPane;
	
	JButton btnIncluir = new JButton("Incluir");
	JButton btnExcluir = new JButton("Excluir");
	JButton btnAlterar = new JButton("Alterar");
	JButton btnConsultar = new JButton("Consultar");
	JButton btnVoltar = new JButton("Voltar");
	
	private final FornecedorDAO fornDao = new FornecedorDAO();

	protected JTextField txtNomeForn;
	protected JTextField txtCNPJ;
	protected JTextField txtEmailForn;
	protected JTextField txtTelefoneForn1;
	protected JTextField txtTelefoneForn2;
	protected JTextField txtEnderecoForn;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmFornecedor frame = new frmFornecedor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmFornecedor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 611, 424);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel txtpainel = new JPanel();
		txtpainel.setLayout(null);
		txtpainel.setBorder(new EmptyBorder(5, 5, 5, 5));
		txtpainel.setBounds(0, 0, 595, 385);
		contentPane.add(txtpainel);

		JLabel lblNewLabel_1 = new JLabel("Nome:");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(23, 61, 46, 14);
		txtpainel.add(lblNewLabel_1);

		txtNomeForn = new JTextField();
		txtNomeForn.setColumns(10);
		txtNomeForn.setBounds(89, 59, 317, 19);
		txtpainel.add(txtNomeForn);

		JLabel lblFornecedor = new JLabel("Fornecedor");
		lblFornecedor.setHorizontalAlignment(SwingConstants.CENTER);
		lblFornecedor.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblFornecedor.setBounds(241, -16, 97, 71);
		txtpainel.add(lblFornecedor);

		JLabel lblNewLabel_2 = new JLabel("CNPJ:");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(23, 102, 46, 14);
		txtpainel.add(lblNewLabel_2);

		txtCNPJ = new JTextField();
		txtCNPJ.setColumns(10);
		txtCNPJ.setBounds(89, 100, 140, 20);
		txtpainel.add(txtCNPJ);

		JLabel lblNewLabel_3 = new JLabel("E-mail:");
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_3.setBounds(23, 142, 46, 14);
		txtpainel.add(lblNewLabel_3);

		txtEmailForn = new JTextField();
		txtEmailForn.setColumns(10);
		txtEmailForn.setBounds(89, 140, 317, 20);
		txtpainel.add(txtEmailForn);

		txtTelefoneForn1 = new JTextField();
		txtTelefoneForn1.setColumns(10);
		txtTelefoneForn1.setBounds(89, 182, 140, 20);
		txtpainel.add(txtTelefoneForn1);

		JLabel lblNewLabel_4 = new JLabel("Telefone:");
		lblNewLabel_4.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_4.setBounds(23, 184, 61, 14);
		txtpainel.add(lblNewLabel_4);

		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnVoltar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnVoltar.setBounds(241, 327, 105, 32);
		txtpainel.add(btnVoltar);

		btnIncluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Fornecedor f = new Fornecedor();
				f.setNomeForn(txtNomeForn.getText());
				f.setCNPJ(txtCNPJ.getText());
				f.setEmailForn(txtEmailForn.getText());
				f.setTelefoneForn1(txtTelefoneForn1.getText());
				f.setTelefoneForn2(txtTelefoneForn2.getText());
				f.setEnderecoForn(txtEnderecoForn.getText());
				try {
					fornDao.Incluir(f);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomeForn.setText("");
				txtCNPJ.setText("");
				txtEmailForn.setText("");
				txtTelefoneForn1.setText("");
				txtTelefoneForn2.setText("");
				txtEnderecoForn.setText("");
			}
		});
		
		btnIncluir.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnIncluir.setBounds(492, 65, 89, 23);
		txtpainel.add(btnIncluir);

		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					fornDao.Excluir(txtNomeForn.getText());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomeForn.setText("");
				txtCNPJ.setText("");
				txtEmailForn.setText("");
				txtTelefoneForn1.setText("");
				txtTelefoneForn2.setText("");
				txtEnderecoForn.setText("");
			}
		});

		btnExcluir.setEnabled(true);
		btnExcluir.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnExcluir.setBounds(492, 105, 89, 23);
		txtpainel.add(btnExcluir);

		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fornecedor f = new Fornecedor();
				f.setNomeForn(txtNomeForn.getText());
				f.setCNPJ(txtCNPJ.getText());
				f.setEmailForn(txtEmailForn.getText());
				f.setTelefoneForn1(txtTelefoneForn1.getText());
				f.setTelefoneForn2(txtTelefoneForn2.getText());
				f.setEnderecoForn(txtEnderecoForn.getText());
				try {
					fornDao.Alterar(f);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomeForn.setText("");
				txtCNPJ.setText("");
				txtEmailForn.setText("");
				txtTelefoneForn1.setText("");
				txtTelefoneForn2.setText("");
				txtEnderecoForn.setText("");
			}
		});
		
		btnAlterar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnAlterar.setBounds(492, 145, 89, 23);
		txtpainel.add(btnAlterar);

		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Fornecedor fornecedor = fornDao.Consultar(txtNomeForn.getText());
					txtNomeForn.setText(fornecedor.getNomeForn());
					txtCNPJ.setText(fornecedor.getCNPJ());
					txtEmailForn.setText(fornecedor.getEmailForn());
					txtTelefoneForn1.setText(fornecedor.getTelefoneForn1());
					txtTelefoneForn2.setText(fornecedor.getTelefoneForn2());
					txtEnderecoForn.setText(fornecedor.getEnderecoForn());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
			}
		});
		
		btnConsultar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnConsultar.setBounds(492, 182, 89, 23);
		txtpainel.add(btnConsultar);

		JLabel lblNewLabel_4_1 = new JLabel("Telefone:");
		lblNewLabel_4_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_4_1.setBounds(23, 226, 61, 14);
		txtpainel.add(lblNewLabel_4_1);

		txtTelefoneForn2 = new JTextField();
		txtTelefoneForn2.setColumns(10);
		txtTelefoneForn2.setBounds(89, 224, 140, 20);
		txtpainel.add(txtTelefoneForn2);

		JLabel lblNewLabel_1_1 = new JLabel("Endere\u00E7o:");
		lblNewLabel_1_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_1_1.setBounds(23, 266, 61, 14);
		txtpainel.add(lblNewLabel_1_1);

		txtEnderecoForn = new JTextField();
		txtEnderecoForn.setColumns(10);
		txtEnderecoForn.setBounds(89, 264, 317, 19);
		txtpainel.add(txtEnderecoForn);
	}

}
