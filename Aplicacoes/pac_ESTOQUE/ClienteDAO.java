package pac_ESTOQUE;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import java.sql.PreparedStatement;

public class ClienteDAO {

	private Connection getConnection() throws SQLException, ClassNotFoundException {
		String driver = "com.mysql.cj.jdbc.Driver";
		String usuario = "root";
		String senha = "yuri2011";
		String url = "jdbc:mysql://localhost:3306/Loja_X?useTimezone=true&serverTimezone=UTC";
		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, usuario, senha);
		return conn;
	}

	public void Incluir(Cliente c) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "insert into "
				+ "Cliente (NomeCli, CpfCli, EmailCli, EnderecoCli, TelefoneCli)"
				+ "values(?, ?, ?, ?, ?)";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, c.getNomeCli());
		comando.setString(2, c.getCpfCli());
		comando.setString(3, c.getEmailCli());
		comando.setString(4, c.getEnderecoCli());
		comando.setString(5, c.getTelefoneCli());
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Cliente inclu�do");
	}

	public void Alterar(Cliente c) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "update Cliente set cpfcli = ?, emailcli = ?, enderecocli = ?,"
				+ "telefonecli = ? where nomecli = ?;";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, c.getCpfCli());
		comando.setString(2, c.getEmailCli());
		comando.setString(3, c.getEnderecoCli());
		comando.setString(4, c.getTelefoneCli());
		comando.setString(5, c.getNomeCli());
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Cliente alterado");
	}

	public void Excluir(String nome) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "delete from Cliente where nomecli = ?;";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, nome);
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Cliente deletado");
	}

	public Cliente Consultar(String nome) throws SQLException, ClassNotFoundException {
		Cliente data = new Cliente();
		Connection conn = getConnection();
		String Sql = "select * from Cliente where nomecli = ?";
		PreparedStatement comando = conn.prepareStatement(Sql);
		comando.setString(1, nome);
		ResultSet result = comando.executeQuery();
		while(result.next()) {
			data.setNomeCli(result.getString("NomeCli"));
			data.setCpfCli(result.getString("CpfCli"));
			data.setEmailCli(result.getString("EmailCli"));
			data.setEnderecoCli(result.getString("EnderecoCli"));
			data.setTelefoneCli(result.getString("TelefoneCli"));
		}
		comando.close();
		conn.close();
		return data;
	}

}
