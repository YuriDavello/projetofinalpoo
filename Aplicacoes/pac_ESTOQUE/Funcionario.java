package pac_ESTOQUE;

public class Funcionario {
	private String nomeFunc;
	private String cpfFunc;
	private String emailFunc;
	private String usuarioFunc;
	private String telefoneFunc;
	
	public String getNomeFunc() {
		return nomeFunc;
	}

	public void setNomeFunc(String nomeFunc) {
		this.nomeFunc = nomeFunc;
	}

	public String getCpfFunc() {
		return cpfFunc;
	}

	public void setCpfFunc(String cpfFunc) {
		this.cpfFunc = cpfFunc;
	}

	public String getEmailFunc() {
		return emailFunc;
	}

	public void setEmailFunc(String emailFunc) {
		this.emailFunc = emailFunc;
	}

	public String getUsuarioFunc() {
		return usuarioFunc;
	}

	public void setUsuarioFunc(String usuarioFunc) {
		this.usuarioFunc = usuarioFunc;
	}


	public String getTelefoneFunc() {
		return telefoneFunc;
	}

	public void setTelefoneFunc(String telefoneFunc) {
		this.telefoneFunc = telefoneFunc;
	}

}
