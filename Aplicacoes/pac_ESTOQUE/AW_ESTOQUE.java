package pac_ESTOQUE;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Font;
import javax.swing.JMenu;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuBar;
import javax.swing.JTextField;
import java.awt.SystemColor;

public class AW_ESTOQUE {

	private JFrame frame;
	private JTextField txtSistemaDeControle;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AW_ESTOQUE window = new AW_ESTOQUE();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AW_ESTOQUE() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 442, 343);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBorderPainted(false);
		menuBar.setFont(new Font("Times New Roman", Font.BOLD, 12));
		menuBar.setBounds(0, 0, 426, 36);
		frame.getContentPane().add(menuBar);

		JMenu menuCadastro = new JMenu("Cadastro");
		menuCadastro.setFont(new Font("Times New Roman", Font.BOLD, 14));
		menuBar.add(menuCadastro);

		JMenu menuCliente = new JMenu("Cliente");
		menuCliente.setFont(new Font("Times New Roman", Font.BOLD, 12));
		menuCliente.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
				frmCliente fc = new frmCliente();
				fc.setVisible(true);
			}
		});
		menuCadastro.add(menuCliente);

		JMenu menuFuncionario = new JMenu("Funcion\u00E1rio");
		menuFuncionario.setFont(new Font("Times New Roman", Font.BOLD, 12));
		menuFuncionario.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frmFuncionario func = new frmFuncionario();
				func.setVisible(true);
			}
		});
		menuCadastro.add(menuFuncionario);

		JMenu menuFornecedor = new JMenu("Fornecedor");
		menuFornecedor.setFont(new Font("Times New Roman", Font.BOLD, 12));
		menuFornecedor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frmFornecedor forn = new frmFornecedor();
				forn.setVisible(true);
			}
		});
		menuCadastro.add(menuFornecedor);

		JMenu menuProduto = new JMenu("Produto");
		menuProduto.setFont(new Font("Times New Roman", Font.BOLD, 12));
		menuProduto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frmProduto p = new frmProduto();
				p.setVisible(true);
			}
		});
		menuCadastro.add(menuProduto);
		
		JMenu mnNewMenu_2 = new JMenu("Sair");
		mnNewMenu_2.setHorizontalAlignment(SwingConstants.LEFT);
		mnNewMenu_2.setFont(new Font("Times New Roman", Font.BOLD, 14));
		menuBar.add(mnNewMenu_2);

		txtSistemaDeControle = new JTextField();
		txtSistemaDeControle.setBackground(SystemColor.menu);
		txtSistemaDeControle.setHorizontalAlignment(SwingConstants.CENTER);
		txtSistemaDeControle.setFont(new Font("Times New Roman", Font.BOLD, 18));
		txtSistemaDeControle.setText("Sistema de Controle de Estoque");
		txtSistemaDeControle.setBounds(48, 103, 318, 109);
		frame.getContentPane().add(txtSistemaDeControle);
		txtSistemaDeControle.setColumns(10);
	}
}
