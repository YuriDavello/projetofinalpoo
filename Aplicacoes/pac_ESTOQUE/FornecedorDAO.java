package pac_ESTOQUE;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import java.sql.PreparedStatement;

public class FornecedorDAO {
	
	private Connection getConnection() throws SQLException, ClassNotFoundException {
		String driver = "com.mysql.cj.jdbc.Driver";
		String usuario = "root";
		String senha = "yuri2011";
		String url = "jdbc:mysql://localhost:3306/Loja_X?useTimezone=true&serverTimezone=UTC";
		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, usuario, senha);
		return conn;
	}
	
	public void Incluir(Fornecedor f) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "insert into "
				+ "Fornecedor (NomeForn, CNPJ, EmailForn, TelefoneForn1, TelefoneForn2, EnderecoForn)"
				+ "values(?, ?, ?, ?, ?, ?)";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, f.getNomeForn());
		comando.setString(2, f.getCNPJ());
		comando.setString(3, f.getEmailForn());
		comando.setString(4, f.getTelefoneForn1());
		comando.setString(5, f.getTelefoneForn2());
		comando.setString(6, f.getEnderecoForn());
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Fornecedor inclu�do");
	}
	
	public void Alterar(Fornecedor f) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "update Fornecedor set cnpj = ?, emailforn = ?, telefoneforn1 = ?,"
				+ "telefoneforn2 = ?, enderecoforn = ? where nomeforn = ?;";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, f.getCNPJ());
		comando.setString(2, f.getEmailForn());
		comando.setString(3, f.getTelefoneForn1());
		comando.setString(4, f.getTelefoneForn2());
		comando.setString(5, f.getEnderecoForn());
		comando.setString(6, f.getNomeForn());
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Fornecedor alterado");
	}
	
	public void Excluir(String nome) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "delete from Fornecedor where nomeforn = ?;";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, nome);
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Fornecedor deletado");
	}
	
	public Fornecedor Consultar(String nome) throws SQLException, ClassNotFoundException {
		Fornecedor data = new Fornecedor();
		Connection conn = getConnection();
		String Sql = "select * from Fornecedor where nomeforn = ?";
		PreparedStatement comando = conn.prepareStatement(Sql);
		comando.setString(1, nome);
		ResultSet result = comando.executeQuery();
		while(result.next()) {
			data.setNomeForn(result.getString("NomeForn"));
			data.setCNPJ(result.getString("CNPJ"));
			data.setEmailForn(result.getString("EmailForn"));
			data.setTelefoneForn1(result.getString("TelefoneForn1"));
			data.setTelefoneForn2(result.getString("TelefoneForn2"));
			data.setEnderecoForn(result.getString("EnderecoForn"));
		}
		comando.close();
		conn.close();
		return data;
	}
	
}
