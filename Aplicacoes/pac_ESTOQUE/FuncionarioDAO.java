package pac_ESTOQUE;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class FuncionarioDAO {
	private Connection getConnection() throws SQLException, ClassNotFoundException {
		String driver = "com.mysql.cj.jdbc.Driver";
		String usuario = "root";
		String senha = "yuri2011";
		String url = "jdbc:mysql://localhost:3306/Loja_X?useTimezone=true&serverTimezone=UTC";
		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, usuario, senha);
		return conn;
	}
	
	public void Incluir(Funcionario f) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "insert into "
				+ "Funcionario (NomeFunc, CpfFunc, Usuario, EmailFunc, TelefoneFunc)"
				+ "values(?, ?, ?, ?, ?)";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, f.getNomeFunc());
		comando.setString(2, f.getCpfFunc());
		comando.setString(3, f.getUsuarioFunc());
		comando.setString(4, f.getEmailFunc());
		comando.setString(5, f.getTelefoneFunc());
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Funcionário incluído");
	}
	
	public void Alterar(Funcionario f) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "update Funcionario set cpffunc = ?, usuario = ?,"
				+ " emailfunc = ?, telefonefunc = ? where nomefunc = ?;";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, f.getCpfFunc());
		comando.setString(2, f.getUsuarioFunc());
		comando.setString(3, f.getEmailFunc());
		comando.setString(4, f.getTelefoneFunc());
		comando.setString(5, f.getNomeFunc());
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Funcionário alterado");
	}
	
	public void Excluir(String nome) throws SQLException, ClassNotFoundException {
		Connection conn = getConnection();
		String Sql = "delete from Funcionario where nomefunc = ?;";
		PreparedStatement comando =  conn.prepareStatement(Sql);
		comando.setString(1, nome);
		comando.execute();
		comando.close();
		conn.close();
		JOptionPane.showMessageDialog(null, "Funcionário deletado");
	}
	
	public Funcionario Consultar(String nome) throws SQLException, ClassNotFoundException {
		Funcionario data = new Funcionario();
		Connection conn = getConnection();
		String Sql = "select * from Funcionario where nomefunc = ?";
		PreparedStatement comando = conn.prepareStatement(Sql);
		comando.setString(1, nome);
		ResultSet result = comando.executeQuery();
		while(result.next()) {
			data.setNomeFunc(result.getString("NomeFunc"));
			data.setCpfFunc(result.getString("CpfFunc"));
			data.setUsuarioFunc(result.getString("Usuario"));
			data.setEmailFunc(result.getString("EmailFunc"));
			data.setTelefoneFunc(result.getString("TelefoneFunc"));
			data.setNomeFunc(result.getString("NomeFunc"));
		}
		comando.close();
		conn.close();
		return data;
	}
}
