package pac_ESTOQUE;

public class Cliente {
	private String nomeCli;
	private String cpfCli;
	private String emailCli;
	private String enderecoCli;
	private String telefoneCli;
	
	public String getNomeCli() {
		return nomeCli;
	}
	public void setNomeCli(String nomeCli) {
		this.nomeCli = nomeCli;
	}
	public String getCpfCli() {
		return cpfCli;
	}
	public void setCpfCli(String cpfCli) {
		this.cpfCli = cpfCli;
	}
	public String getEmailCli() {
		return emailCli;
	}
	public void setEmailCli(String emailCli) {
		this.emailCli = emailCli;
	}
	public String getEnderecoCli() {
		return enderecoCli;
	}
	public void setEnderecoCli(String enderecoCli) {
		this.enderecoCli = enderecoCli;
	}
	public String getTelefoneCli() {
		return telefoneCli;
	}
	public void setTelefoneCli(String telefoneCli) {
		this.telefoneCli = telefoneCli;
	}
	
	

}
