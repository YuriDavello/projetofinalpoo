package pac_ESTOQUE;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

@SuppressWarnings({ "serial", "unused" })

public class frmCliente extends JFrame {

	protected JPanel contentPane;
	JButton btnIncluir = new JButton("Incluir");
	JButton btnExcluir = new JButton("Excluir");
	JButton btnAlterar = new JButton("Alterar");
	JButton btnConsultar = new JButton("Consultar");
	JButton btnVoltar = new JButton("Voltar");
	
	private final ClienteDAO clienteDao = new ClienteDAO();

	protected JTextField txtNomeCli;
	protected JTextField txtCpfCli;
	protected JTextField txtEmailCli;
	protected JTextField txtEnderecoCli;
	protected JTextField txtTelefoneCli;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmCliente frame = new frmCliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmCliente() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 614, 409);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Nome:");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(23, 61, 46, 14);
		contentPane.add(lblNewLabel_1);

		txtNomeCli = new JTextField();
		txtNomeCli.setColumns(10);
		txtNomeCli.setBounds(89, 59, 317, 19);
		contentPane.add(txtNomeCli);

		JLabel lblNewLabel = new JLabel("Cliente");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel.setBounds(241, -16, 97, 71);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_2 = new JLabel("CPF:");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(23, 102, 46, 14);
		contentPane.add(lblNewLabel_2);

		txtCpfCli = new JTextField();
		txtCpfCli.setColumns(10);
		txtCpfCli.setBounds(89, 100, 140, 20);
		contentPane.add(txtCpfCli);

		JLabel lblNewLabel_3 = new JLabel("E-mail:");
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_3.setBounds(23, 142, 46, 14);
		contentPane.add(lblNewLabel_3);

		txtEmailCli = new JTextField();
		txtEmailCli.setColumns(10);
		txtEmailCli.setBounds(89, 140, 317, 20);
		contentPane.add(txtEmailCli);

		JLabel lblNewLabel_5 = new JLabel("Endere\u00E7o:");
		lblNewLabel_5.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_5.setBounds(23, 182, 71, 14);
		contentPane.add(lblNewLabel_5);

		txtEnderecoCli = new JTextField();
		txtEnderecoCli.setColumns(10);
		txtEnderecoCli.setBounds(89, 180, 317, 20);
		contentPane.add(txtEnderecoCli);

		txtTelefoneCli = new JTextField();
		txtTelefoneCli.setColumns(10);
		txtTelefoneCli.setBounds(89, 221, 140, 20);
		contentPane.add(txtTelefoneCli);

		JLabel lblNewLabel_4 = new JLabel("Telefone:");
		lblNewLabel_4.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_4.setBounds(23, 223, 61, 14);
		contentPane.add(lblNewLabel_4);


		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnVoltar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnVoltar.setBounds(241, 312, 105, 32);
		contentPane.add(btnVoltar);

		btnIncluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Cliente c = new Cliente();
				c.setNomeCli(txtNomeCli.getText());
				c.setCpfCli(txtCpfCli.getText());
				c.setEmailCli(txtEmailCli.getText());
				c.setEnderecoCli(txtEnderecoCli.getText());
				c.setTelefoneCli(txtTelefoneCli.getText());
				try {
					clienteDao.Incluir(c);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomeCli.setText("");
				txtCpfCli.setText("");
				txtEmailCli.setText("");
				txtEnderecoCli.setText("");
				txtTelefoneCli.setText("");
			}
		});

		btnIncluir.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnIncluir.setBounds(492, 65, 89, 23);
		contentPane.add(btnIncluir);

		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					clienteDao.Excluir(txtNomeCli.getText());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomeCli.setText("");
				txtCpfCli.setText("");
				txtEmailCli.setText("");
				txtEnderecoCli.setText("");
				txtTelefoneCli.setText("");
			}
		});


		btnExcluir.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnExcluir.setBounds(492, 105, 89, 23);
		contentPane.add(btnExcluir);

		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cliente c = new Cliente();
				c.setNomeCli(txtNomeCli.getText());
				c.setCpfCli(txtCpfCli.getText());
				c.setEmailCli(txtEmailCli.getText());
				c.setEnderecoCli(txtEnderecoCli.getText());
				c.setTelefoneCli(txtTelefoneCli.getText());
				try {
					clienteDao.Alterar(c);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomeCli.setText("");
				txtCpfCli.setText("");
				txtEmailCli.setText("");
				txtEnderecoCli.setText("");
				txtTelefoneCli.setText("");
			}
		});


		btnAlterar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnAlterar.setBounds(492, 145, 89, 23);
		contentPane.add(btnAlterar);

		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Cliente cliente = clienteDao.Consultar(txtNomeCli.getText());
					txtNomeCli.setText(cliente.getNomeCli());
					txtCpfCli.setText(cliente.getCpfCli());
					txtEmailCli.setText(cliente.getEmailCli());
					txtEnderecoCli.setText(cliente.getEnderecoCli());
					txtTelefoneCli.setText(String.valueOf(cliente.getTelefoneCli()));
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
			}
		});
		
		btnConsultar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnConsultar.setBounds(492, 182, 89, 23);
		contentPane.add(btnConsultar);
	}

}
