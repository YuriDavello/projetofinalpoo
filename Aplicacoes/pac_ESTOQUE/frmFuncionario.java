package pac_ESTOQUE;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;

@SuppressWarnings({ "serial", "unused" })

public class frmFuncionario extends JFrame {

	protected JPanel contentPane;
	
	JButton btnIncluir = new JButton("Incluir");
	JButton btnExcluir = new JButton("Excluir");
	JButton btnAlterar = new JButton("Alterar");
	JButton btnConsultar = new JButton("Consultar");
	JButton btnVoltar = new JButton("Voltar");
	
	private final FuncionarioDAO funcDao = new FuncionarioDAO();

	protected JTextField txtNomeFunc;
	protected JTextField txtCpfFunc;
	protected JTextField txtEmailFunc;
	protected JTextField txtTelefoneFunc;
	protected JTextField txtUsuarioFunc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmFuncionario frame = new frmFuncionario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmFuncionario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 618, 402);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel txtpainel = new JPanel();
		txtpainel.setLayout(null);
		txtpainel.setBorder(new EmptyBorder(5, 5, 5, 5));
		txtpainel.setBounds(0, 0, 598, 370);
		contentPane.add(txtpainel);

		JLabel lblNewLabel_1 = new JLabel("Nome:");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(23, 61, 46, 14);
		txtpainel.add(lblNewLabel_1);

		txtNomeFunc = new JTextField();
		txtNomeFunc.setColumns(10);
		txtNomeFunc.setBounds(89, 59, 317, 19);
		txtpainel.add(txtNomeFunc);

		JLabel lblFuncionrio = new JLabel("Funcion\u00E1rio");
		lblFuncionrio.setHorizontalAlignment(SwingConstants.CENTER);
		lblFuncionrio.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblFuncionrio.setBounds(241, -16, 97, 71);
		txtpainel.add(lblFuncionrio);

		JLabel lblNewLabel_2 = new JLabel("CPF:");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(23, 102, 46, 14);
		txtpainel.add(lblNewLabel_2);

		txtCpfFunc = new JTextField();
		txtCpfFunc.setColumns(10);
		txtCpfFunc.setBounds(89, 100, 140, 20);
		txtpainel.add(txtCpfFunc);

		JLabel lblNewLabel_3 = new JLabel("E-mail:");
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_3.setBounds(23, 142, 46, 14);
		txtpainel.add(lblNewLabel_3);

		txtEmailFunc = new JTextField();
		txtEmailFunc.setColumns(10);
		txtEmailFunc.setBounds(89, 140, 317, 20);
		txtpainel.add(txtEmailFunc);

		txtTelefoneFunc = new JTextField();
		txtTelefoneFunc.setColumns(10);
		txtTelefoneFunc.setBounds(89, 182, 140, 20);
		txtpainel.add(txtTelefoneFunc);

		JLabel lblNewLabel_4 = new JLabel("Telefone:");
		lblNewLabel_4.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_4.setBounds(23, 184, 61, 14);
		txtpainel.add(lblNewLabel_4);

		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnVoltar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnVoltar.setBounds(241, 312, 105, 32);
		txtpainel.add(btnVoltar);

		btnIncluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Funcionario f = new Funcionario();
				f.setNomeFunc(txtNomeFunc.getText());
				f.setCpfFunc(txtCpfFunc.getText());
				f.setEmailFunc(txtEmailFunc.getText());
				f.setUsuarioFunc(txtUsuarioFunc.getText());
				f.setTelefoneFunc(txtTelefoneFunc.getText());
				try {
					funcDao.Incluir(f);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomeFunc.setText("");
				txtCpfFunc.setText("");
				txtEmailFunc.setText("");
				txtTelefoneFunc.setText("");
				txtUsuarioFunc.setText("");
			}
		});
		
		btnIncluir.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnIncluir.setBounds(492, 65, 89, 23);
		txtpainel.add(btnIncluir);

		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					funcDao.Excluir(txtNomeFunc.getText());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomeFunc.setText("");
				txtCpfFunc.setText("");
				txtEmailFunc.setText("");
				txtTelefoneFunc.setText("");
				txtUsuarioFunc.setText("");
			}
		});
		
		btnExcluir.setEnabled(true);
		btnExcluir.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnExcluir.setBounds(492, 105, 89, 23);
		txtpainel.add(btnExcluir);

		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Funcionario f = new Funcionario();
				f.setNomeFunc(txtNomeFunc.getText());
				f.setCpfFunc(txtCpfFunc.getText());
				f.setEmailFunc(txtEmailFunc.getText());
				f.setUsuarioFunc(txtUsuarioFunc.getText());
				f.setTelefoneFunc(txtTelefoneFunc.getText());
				try {
					funcDao.Alterar(f);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
				txtNomeFunc.setText("");
				txtCpfFunc.setText("");
				txtEmailFunc.setText("");
				txtTelefoneFunc.setText("");
				txtUsuarioFunc.setText("");
			}
		});

		btnAlterar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnAlterar.setBounds(492, 145, 89, 23);
		txtpainel.add(btnAlterar);

		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Funcionario funcionario = funcDao.Consultar(txtNomeFunc.getText());
					txtNomeFunc.setText(funcionario.getNomeFunc());
					txtCpfFunc.setText(funcionario.getCpfFunc());
					txtEmailFunc.setText(funcionario.getEmailFunc());
					txtTelefoneFunc.setText(funcionario.getTelefoneFunc());
					txtUsuarioFunc.setText(funcionario.getUsuarioFunc());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
			}
		});
		
		btnConsultar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnConsultar.setBounds(492, 182, 89, 23);
		txtpainel.add(btnConsultar);

		JLabel lblNewLabel_4_1 = new JLabel("Usuario:");
		lblNewLabel_4_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_4_1.setBounds(23, 226, 61, 14);
		txtpainel.add(lblNewLabel_4_1);

		txtUsuarioFunc = new JTextField();
		txtUsuarioFunc.setColumns(10);
		txtUsuarioFunc.setBounds(89, 224, 140, 20);
		txtpainel.add(txtUsuarioFunc);
	}

}
